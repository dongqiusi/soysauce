import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

# data preparation
data_2013 = pd.read_csv("data/2013.csv", sep='\t', names=["Year", "Country", "Popularity"])
data_2014 = pd.read_csv("data/2014.csv", sep='\t', names=["Year", "Country", "Popularity"])
data_2015 = pd.read_csv("data/2015.csv", sep='\t', names=["Year", "Country", "Popularity"])
data = pd.concat([data_2013, data_2014, data_2015])
data.Year = data.Year.astype(str)
data.Popularity = data.Popularity.astype(float)


# data collection and plotting
fig = plt.figure(figsize=(8, 6), dpi=80)

countries = data["Country"].unique()
for country in countries:
	country_data = data[data["Country"] == country]
	plt.ylim(0, 0.6)
	ax = plt.gca()
	vals = ax.get_yticks()
	ax.set_yticklabels(['{:,.0%}'.format(x) for x in vals])
	plt.plot(country_data["Year"], country_data["Popularity"], label='Inline label')
	plt.xlabel("Year", fontsize=14)
	plt.ylabel("Popularity %", fontsize=14)
	
plt.legend(data["Country"].unique(), loc='center left', bbox_to_anchor=(1.04, 0.5), fontsize=12)
plt.title("China's Top Destinations for Road Trip Overseas", fontsize=14, pad=50)
plt.tick_params(top=False, bottom=False, left=False, right=False, labelleft=True, labelbottom=True,
				labelsize="medium", colors="grey")
for spine in plt.gca().spines.values():
    spine.set_visible(False)
plt.tight_layout()
plt.show()